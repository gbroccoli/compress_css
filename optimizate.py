import csscompressor
import os

def compress_css(input_file, output_file):
    """_Функция для сжатия CSS файла_

    Args:
        Args:
        input_file (_str_): _Путь к исходному CSS файлу_.
        output_file (_str_): _Путь к сжатому CSS файлу_.
    """
    try:
        if input_file.endswith(".css") and os.path.isdir(input_file) != True:
            with open(input_file, 'r') as f:
                css_content = f.read()

            compressed_css = csscompressor.compress(css_content)

            with open(output_file, 'w') as f:
                f.write(compressed_css)

            print(f"CSS файл успешно сжат и сохранен в {output_file}")
    except Exception as e:
        print(f"Произошла ошибка: {e}")


# Пример использования:
input_directory = "C:\\Users\\zerat\\Desktop\\testCss"
input_files = os.listdir(input_directory)

if not os.path.exists(f"{input_directory}\\optimaze"):
    os.makedirs(f"{input_directory}\\optimaze")

optimaze_directory = os.path.join(input_directory, "optimaze")

for file in input_files:
    input_file_path = os.path.join(input_directory, file)
    output_file = os.path.join(optimaze_directory, f"{file[:-4]}.min.css")
    compress_css(input_file_path, output_file)
